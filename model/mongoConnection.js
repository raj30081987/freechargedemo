module.exports = {

     countDocuments: async (collection, jsonwhr) => {
        try {
            let db = app.get('mongodb');
            let numOfDocs = await db.collection(collection).countDocuments(jsonwhr);
            return numOfDocs;
        } catch (error) {
            throw new Error(error)
        }
    },

     updateOne: async (collection, filter, update) => {
        try {
            let db = app.get('mongodb');
            let result = await db.collection(collection).updateOne(filter, { $set: update });
            return result.result;
        } catch (error) {
            throw new Error(error)
        }
    },

     insertMany: async (collection, docs) => {
        try {
            let db = app.get('mongodb');
            let result = await db.collection(collection).insertMany(docs);
            return result.result;
        } catch (error) {
            throw new Error(error)
        }
    },

     insert: async (collection, docs) => {
        try {
            let db = app.get('mongodb');
            let result = await db.collection(collection).insertOne(docs);
            return result.result;
        } catch (error) {
            throw new Error(error)
        }
    },

     find: async (collection, query) => {
        try {
            let db = app.get('mongodb');
            let result = await db.collection(collection).findOne(query);
  
            return result;
        } catch (error) { 
            console.log(error);
            
            throw new Error(error)
        }
    } 
 
}