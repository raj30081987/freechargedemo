var express = require('express');
var router = express.Router();
var auth = require('../auth/tokenManagement');
var userController = require('../controller/userController');

/* GET users listing. */

router.post('/register', userController.register);

router.post('/login', userController.login);

router.get('/refreshtoken',auth.validateToken,userController.refreshToken);

module.exports = router;
