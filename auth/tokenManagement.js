var jwt = require('jsonwebtoken');
var sendResponse = require('../model/send');
var private_key = 'freechargedemo'
var expires_in =900; //15 min
module.exports = {
    generateToken: function (userName,password) {
        try {
            var claims = {
                sub: 'authentication',
                iss: 'http://freecharge.com',
                permissions: 'all',
                identity: {
                    source: 'freecharge',
                    userName: userName,
                    password: password,
                    date: new Date()
                }
            };
            var token = jwt.sign({ exp: Math.floor(Date.now() / 1000) + expires_in, data: claims }, private_key);
            
            return token;
        } catch (error) {
            console.log(error);
            
        }
      
    },
    validateToken: function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, private_key, function (err, decoded) {
                if (err) {
                    console.log(JSON.stringify(err));
                    sendResponse.sendCustomJSON(res, false, {}, 'Failed to authenticate token.')
                } else {
                    // if everything is good, save to request for use in other routes
                    //console.log(JSON.stringify(decoded));
                    req.decoded = decoded;
                    return next();
                }
            });
        } else {
            // if there is no token
            // return an error
            sendResponse.sendCustomJSON(res, false, {}, 'Please provide the access token.');
        }
    }  
}; 
