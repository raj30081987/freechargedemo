var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('config');
var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');

var users = require('./routes/users');

app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

MongoClient.connect(config.mongo_connection.connection_string, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
    if (!err) {
        // dmp system
        app.set('mongodb', client.db(config.mongo_connection.db_name));
        assert.equal(null, err);
        console.log("Mongo DB Instance Connected Succesfully."); 
    } else {
        console.log("Mongo DB Instance Not Connected.");
        console.log(err);
    }
});

module.exports = app;
