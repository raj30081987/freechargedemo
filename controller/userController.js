var sendResponse = require('../model/send');
var mongoPromise = require('../model/mongoConnection');
var auth = require('../auth/tokenManagement');
module.exports = {
    register: async function (req, res) {

        let userName = req.body.userName ? req.body.userName : null;
        let password = req.body.password ? req.body.password : null;
        let firstName = req.body.firstName ? req.body.firstName : null;
        let lastName = req.body.lastName ? req.body.lastName : null;
        let emailId = req.body.emailId ? req.body.emailId : null;
        let gender = req.body.gender ? req.body.gender : null;
        let dateTime = new Date();

        if (userName && password) {
            try {
                let pcount = await mongoPromise.countDocuments('user_master', { userName });
                if (pcount > 0) {
                    sendResponse.sendCustomJSON(res, false, {}, "User already available with this userId!");
                } else {
                    let result = await mongoPromise.insert('user_master',
                        {
                            userName, password, firstName, lastName, emailId, gender,
                            registerDate: dateTime
                        }
                    );
                    console.log(result);
                    sendResponse.sendCustomJSON(res, true, { result }, "User Successfully Created!");
                }
            } catch (error) {
                console.log(error);
                sendResponse.sendCustomJSON(res, false, {}, "Something got wrong!");
            }
        } else {
            sendResponse.sendCustomJSON(res, false, {}, "Username / Password required to create new user!");
        }
    },

    login: async function (req, res) {
        let userName = req.body.userName ? req.body.userName : null;
        let password = req.body.password ? req.body.password : null;
        if (userName && password) {
            try {
                let userDetails = await mongoPromise.find('user_master', { userName: userName, password: password });
                if (userDetails != null && userDetails != undefined && userDetails.userName != "") {
                    //login successfull 
                    var token = auth.generateToken(userName, password);
                    userDetails.authToken = token;
                    delete userDetails._id;
                    delete userDetails.password;
                    sendResponse.sendCustomJSON(res, true, userDetails, "User Details found!");
                } else {
                    sendResponse.sendCustomJSON(res, false, {}, "Username/Password Incorrect!");
                }
            } catch (error) {  
                sendResponse.sendCustomJSON(res, false, {}, "Something got wrong!");
            }
        } else {
            sendResponse.sendCustomJSON(res, false, {}, "Username/Password Incorrect!");
        }
    },

    refreshToken :async function(req,res){
      var userName =  req.decoded.data.identity.userName;
      var password =  req.decoded.data.identity.password;
      var newToken = auth.generateToken(userName,password);
      sendResponse.sendCustomJSON(res, true, { newToken :newToken }, "New access token generated.");
    }
}